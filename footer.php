			</div> <!-- end of #main .left -->
			
			<div class="right">
				<div class="widget green kontakt">
					<h3>Kontaktné informácie</h3>
					<div class="main">
						<?php $id = 11; $post = get_post($id); setup_postdata($post);?>
						<?php the_content();?>
						<?php edit_post_link('Edituj.', '<p>', '</p>', $id);?>
					</div>
				</div>
				
				<div class="widget red referencie">
					<h3>Referencie</h3>
					
					<div class="main">
						<?php $id = 14; $post = get_post($id); setup_postdata($post);?>
						<?php the_content();?>
						<?php edit_post_link('Edituj.', '<p>', '</p>', $id);?>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			<div class="bottom">&nbsp;</div>
		</div> <!-- end of #main -->
		
		<div id="footer">
			<div class="left">
				<span>(c) 2009 - <?php echo date('Y');?> <strong>B.P.O.</strong></span> - Bezpečnosť práce a požiarna ochrana
			</div>
			
			<div class="right">
					<a href="<?php echo get_permalink(20);?>">mapa stránok</a> | 
					<a href="<?php echo get_permalink(10);?>">kontaktné informácie</a> |
					<a href="#header">hore</a>
			</div>
		</div>
	</div> <!-- end of #container -->
<script type="text/javascript">
	function init() {
		P7_ExpMenu();
	}
	window.onload = init;
</script>
<?php wp_footer(); ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17033716-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> 
</body>
</html>
