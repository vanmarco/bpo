<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/reset.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />


<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="container">
	<div id="header">
		<div class="top">
			<div class="left">
				<span>B.P.O.</span> - Bezpečnosť práce a požiarna ochrana
			</div>
			
			<div class="right">
				mobil: <span>0905 200 658</span> | tel./fax: <span>02/526 23 961</span> | email: <a href="mailto:bpo&#64;bpo.sk">bpo&#64;bpo.sk</a>
			</div>
		</div>
		<div class="middle">
			<a href="<?php bloginfo('url');?>"><img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo('description'); ?>" /></a>
		</div>
		<div id="nav-menu">
			<?php bpo_get_menu();?>
		</div>
		
	</div>
	<div id="main">
	
		<div class="left content">
			