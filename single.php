<?php
get_header();
?>

<?php the_post();?>

<h1><?php the_title(); ?></h1>

<?php the_content();?>

						
						<?php if(is_page()) { ?>
							<?php $page_list = wp_list_pages('echo=0&title_li=&child_of='.$post->ID);?>
							<?php if($page_list) { ?>
								<div class="sub-pages">
									<h3>Podstránky</h3>
									<ul>
										<?php echo $page_list; ?>	
									</ul>
								</div>
							<?php } ?>
						<?php } ?>

<?php edit_post_link('Edituj túto stránku.', '<p>', '</p>');?>

<?php get_footer(); ?>
