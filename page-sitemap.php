<?php 
/*
Template name: Mapa stránky
*/

get_header();
?>

<?php the_post();?>

<h1><?php the_title(); ?></h1>

<?php the_content();?>

<ul>
	<?php wp_list_pages('title_li=&exclude=11,14,20&order_by=menu_order'); ?>
</ul>

<?php edit_post_link('Edituj túto stránku.', '<p>', '</p>');?>

<?php get_footer(); ?>
