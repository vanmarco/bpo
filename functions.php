<?php

add_theme_support('post-thumbnails');

function bpo_add_js() {
	wp_enqueue_script('p7exp', get_bloginfo('template_url') . '/js/p7exp.js', false, false, true);
}
add_action('template_redirect', 'bpo_add_js');

function bpo_get_menu() { ?>
	<ul id="nav-menu-ul">
		<?php//  if(is_home()) { class="current" }><a href="<?php bloginfo('url');/">Hlavná stránka</a></li> ?>
		<?php echo bpo_get_page_link(3);?>
        <?php echo bpo_get_page_link(419);?>
		<?php echo bpo_get_page_link(368);?>
		<?php echo bpo_get_page_link(4);?>
		<?php echo bpo_get_page_link(6);?>
		<?php echo bpo_get_page_link(8);?>
		<?php echo bpo_get_page_link(9);?>
                <?php echo bpo_get_page_link(7);?>
		<?php echo bpo_get_page_link(10);?>
		<?php // echo beb_get_cat_link(5);?>
		<?php //echo bpo_get_page_link(92)?>
		<?php //echo bpo_get_page_link(6)?>
	</ul>
<?php }

function bpo_get_cat_link($id) {
	$class = '';
	if(is_category($id) || in_category($id)) {
		$class = ' class="current"';
	}
	return sprintf('<li%s><a href="%s">%s</a></li>', $class, get_category_link($id), get_cat_name($id));
}

function bpo_get_page_link($id) {
	global $post;
	$class = '';
        $post_meta_data = get_post_meta($id, 'menu_link', true);
        $link_title = empty($post_meta_data) ? get_the_title($id) : $post_meta_data;

	if(is_page($id) || (is_page() && in_array($id, get_post_ancestors($post->ID)))) {
		$class = ' class="current"';
	}
	$children = wp_list_pages("title_li=&depth=1&echo=0&sort_column=menu_order&child_of=$id");
	if(!empty($children)) {
		$children = '<ul>' . $children . '</ul>';
	}
	return sprintf('<li%s><a href="%s">%s</a>%s</li>', $class, get_permalink($id), $link_title, $children);
}

function bpo_title($title) {
	global $post;

	$ancestors = get_post_ancestors($post->ID);

	if(is_page() && !is_front_page() && $ancestors) {
		$title = esc_attr(get_the_title($post->ID));
		foreach($ancestors as $ancestor) {
			$title .= " | " . esc_attr(get_the_title($ancestor));
		}
	}

	return $title;
}
add_filter('single_post_title', 'bpo_title');
